﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpacemanTumble : MonoBehaviour
{
    public float tumble;
    public float speed;
    Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.angularVelocity = Random.insideUnitSphere * tumble;
    }
    void FixedUpdate()
    {
        rb.velocity = new Vector3(0,0,1) * speed;
    }
}
