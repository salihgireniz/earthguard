﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public float speedIncrement;

    public GameObject[] Sets;
    public float startWait;
    public float setWait;
    public int waveCount;
    public float astreoidTime;
    private Vector3 spawnPos;
    public bool astreoidStarted;

    

    public GameObject[] hazards;
    public float hazardCount;
    public float spawnWait;
    private Vector3 spawnValues;

    void Awake()
    {
        instance = this;
    }
    void Start()
    {
        astreoidStarted = false;
        speedIncrement = 1f;
        spawnPos = new Vector3(0, 0, 5f);
        spawnValues = new Vector3(6, 0, 25f);
        StartCoroutine(WaveSpawner());
    }
    void Update()
    {
        if (astreoidStarted == true)
        {
            StartCoroutine(AstreoidStarter());
        }
    }

    IEnumerator WaveSpawner()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for(int i = 0; i < waveCount; i++)
            {
                GameObject Set = Sets[Random.Range(0, Sets.Length)];
                Instantiate(Set, spawnPos, Quaternion.identity);
                yield return new WaitForSeconds(setWait);
            }
            astreoidStarted = true;
            yield return new WaitForSeconds(astreoidTime);
            waveCount++;
        }
    }
    IEnumerator AstreoidStarter()
    {
        astreoidStarted = false;
        yield return new WaitForSeconds(startWait);
        for (int i = 0; i < hazardCount; i++)
        {
            GameObject hazard = hazards[Random.Range(0, hazards.Length)];
            Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
            Quaternion spawnRotation = Quaternion.identity;
            Instantiate(hazard, spawnPosition, spawnRotation);
            yield return new WaitForSeconds(spawnWait);
        }
    }
 }

