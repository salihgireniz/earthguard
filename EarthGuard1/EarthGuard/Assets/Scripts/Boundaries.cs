﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boundaries : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Dragger")
        {
            Destroy(other.gameObject);
        }
    }
}
