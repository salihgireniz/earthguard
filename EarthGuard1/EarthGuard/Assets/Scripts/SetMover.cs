﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetMover : MonoBehaviour
{
    private Rigidbody rb;
    public float speed;
    public float speedIncrement;
    void Start()
    {
        speedIncrement = GameManager.instance.speedIncrement;
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = -new Vector3(0, 0, 1) * speed* speedIncrement;
    }
}
