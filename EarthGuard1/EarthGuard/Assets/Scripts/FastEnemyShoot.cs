﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FastEnemyShoot : MonoBehaviour
{
    private float nextFire;
    public float fireRate;
    public GameObject shot;
    private Vector3 shotPos;
    private Quaternion shotRot;

    void Start()
    {
        
        shotRot = Quaternion.Euler(-90f,0,0);
    }
    void Update()
    {
        shotPos = new Vector3(transform.position.x, transform.position.y, transform.position.z - 2f);
        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(shot, shotPos, shotRot);
        }

    }
}
