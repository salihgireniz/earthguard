﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketMover : MonoBehaviour
{
    private Rigidbody rb;
    public float speed;
    public float speedIncrement;
    void Start()
    {
        speedIncrement = GameManager.instance.speedIncrement;
        speed = 5f;
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.velocity = transform.up * speed * speedIncrement;;
    }
}
