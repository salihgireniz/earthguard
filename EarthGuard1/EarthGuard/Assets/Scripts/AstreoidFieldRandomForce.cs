﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AstreoidFieldRandomForce : MonoBehaviour
{
    private Vector3 force;
    private Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        StartCoroutine(AddRandomForce());
    }

    // Update is called once per frame
    IEnumerator AddRandomForce()
    {
        force = new Vector3(Random.Range(-300,300), 0, Random.Range(-300, 300));
        rb.AddForce(force);
        yield return new WaitForSeconds(2);
        Destroy(gameObject);
    }
}
