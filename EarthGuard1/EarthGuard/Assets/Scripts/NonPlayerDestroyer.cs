﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NonPlayerDestroyer : MonoBehaviour
{
    public int ShotNumToDie;
    public GameObject HitEffect;
    public GameObject DestroyedEffect;
    private int i = 0;
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Laser")
        {
            if (i < ShotNumToDie)
            {
                Instantiate(HitEffect, other.transform.position, other.transform.rotation);
                Destroy(other.gameObject);
                i++;
                return;
            }
            Instantiate(DestroyedEffect, transform.position, transform.rotation);
            Destroy(gameObject);

        }

        
    }
}
