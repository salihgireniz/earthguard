﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowEnemyShoot : MonoBehaviour
{
    private float nextFire;
    public float fireRate;
    public GameObject shot;
    private Vector3 shotPos;
    private Quaternion shotRot;

    void Start()
    {
        
        shotRot = Quaternion.Euler(-90f, 0, 0);
    }
    IEnumerator SlowShoot()
    {
        yield return new WaitForSeconds(6);
        Instantiate(shot, shotPos, shotRot);
    }
    void Update()
    {
        shotPos = new Vector3(transform.position.x, transform.position.y, transform.position.z - 2f);
        if (Time.time > nextFire || Time.time == nextFire)
        {
            nextFire = Time.time + fireRate;
            StartCoroutine(SlowShoot());
        }
        
    }
}
