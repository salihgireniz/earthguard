﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceManThrow : MonoBehaviour
{
    private float nextFire;
    public float throwRate;
    public GameObject Spaceman;
    private Vector3 shotPos;
    private Quaternion shotRot;

    void Start()
    {
        
        shotRot = Quaternion.Euler(90, 0, 0);
    }
    IEnumerator SlowShoot()
    {
        yield return new WaitForSeconds(4);
        Instantiate(Spaceman, shotPos, shotRot);
    }
    void Update()
    {
        shotPos = new Vector3(transform.position.x, transform.position.y, transform.position.z - 2f);
        if (Time.time > nextFire || Time.time == nextFire)
        {
            nextFire = Time.time + throwRate;
            StartCoroutine(SlowShoot());
        }

    }
}
