﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    private float nextFire;
    public float fireRate;
    public GameObject shot;
    private Vector3 shotPos;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        shotPos = new Vector3(transform.position.x, transform.position.y - 0.722f, transform.position.z + 1f);
        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(shot, shotPos, transform.rotation);

        }
    }
}
